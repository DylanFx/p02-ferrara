//
//  TileView.h
//  p02-ferrara
//
//  Created by Dylan Ferrara on 2/13/17.
//  Copyright © 2017 Dylan Ferrara. All rights reserved.
//

#ifndef TileView_h
#define TileView_h

#import <UIKit/UIKit.h>

@interface TileView : UIView

@property (nonatomic, strong) UILabel *label;
@property (nonatomic) int value;

- (void)updateLabel;

@end

#endif /* TileView_h */
