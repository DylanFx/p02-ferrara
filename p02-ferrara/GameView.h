//
//  GameView.h
//  p02-ferrara
//
//  Created by Dylan Ferrara on 2/13/17.
//  Copyright © 2017 Dylan Ferrara. All rights reserved.
//

#ifndef GameView_h
#define GameView_h

#import <UIKit/UIKit.h>
#import "TileView.h"

@interface GameView : UIView

@property (nonatomic, strong) NSArray *directions;
@property (nonatomic, strong) NSMutableArray *tiles;

@end

#endif /* GameView_h */
