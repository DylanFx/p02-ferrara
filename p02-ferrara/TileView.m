//
//  TileView.m
//  p02-ferrara
//
//  Created by Dylan Ferrara on 2/13/17.
//  Copyright © 2017 Dylan Ferrara. All rights reserved.
//

#import "TileView.h"

@implementation TileView

@synthesize label;
@synthesize value;

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    
    if (self) {
        label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)];
        [self addSubview:label];
        [label setText:@""];
        [label setTextAlignment:NSTextAlignmentCenter];
        [label setFont:[UIFont fontWithName:@"HelveticaNeue-Bold" size:25]];
        [label setTextColor:[UIColor blackColor]];
        [self setBackgroundColor:[UIColor whiteColor]];
    }
    return self;
}

- (void)updateLabel {
    if (value == 0) {
        [label setText:@""];
        [label setBackgroundColor:[UIColor whiteColor]];
    } else {
        [label setText:[NSString stringWithFormat:@"%d", value]];
        switch (value) {
            case 2:
                [label setTextColor:[UIColor blackColor]];
                [label setBackgroundColor:[UIColor colorWithRed:238./255. green:228./255. blue:218./255. alpha:1]];
                break;
            case 4:
                [label setTextColor:[UIColor blackColor]];
                [label setBackgroundColor:[UIColor colorWithRed:237./255. green:224./255. blue:200./255. alpha:1]];
                break;
            case 8:
                [label setTextColor:[UIColor whiteColor]];
                [label setBackgroundColor:[UIColor colorWithRed:242./255. green:177./255. blue:121./255. alpha:1]];
                break;
            case 16:
                [label setTextColor:[UIColor whiteColor]];
                [label setBackgroundColor:[UIColor colorWithRed:245./255. green:149./255. blue:99./255. alpha:1]];
                break;
            case 32:
                [label setTextColor:[UIColor whiteColor]];
                [label setBackgroundColor:[UIColor colorWithRed:246./255. green:124./255. blue:95./255. alpha:1]];
                break;
            case 64:
                [label setTextColor:[UIColor whiteColor]];
                [label setBackgroundColor:[UIColor colorWithRed:246./255. green:94./255. blue:59./255. alpha:1]];
                break;
            case 128:
            case 256:
            case 512:
            case 1024:
            case 2048:
                [label setTextColor:[UIColor whiteColor]];
                [label setBackgroundColor:[UIColor colorWithRed:237./255. green:207./255. blue:114./255. alpha:1]];
                break;
            default:
                [label setBackgroundColor:[UIColor whiteColor]];
                break;
        }
    }
}
@end
